﻿using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace Redis
{
    public interface IDbCacheService
    {
        Task<bool> StringSetAsync(string key, object value, TimeSpan? expireTime = null,
            When when = When.Always, CommandFlags commandFlags = CommandFlags.None);

        bool StringSet(string key, object value);

        Task<object> StringGetAsync(string key);

        object StringGet(string key);

        Task<bool> KeyExistAsync(string key);

        bool KeyExist(string key);

        Task<bool> KeyDeleteAsync(string key);

        bool KeyDelete(string key);

        Task<long> StringAppendAsync(string key, object value);

        long StringAppend(string key, object value);

        Task<object> StringGetSetAsync(string key, object value);

        object StringGetSet(string key, object value);
    }
}
