﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace Redis
{
    public class DbCacheService : IDbCacheService
    {
        protected readonly IConnectionMultiplexer ConnectionMultiplexer;

        protected readonly IDatabase Database;

        protected readonly IServer Server;

        public DbCacheService()
        {
            ConnectionMultiplexer = StackExchange.Redis.ConnectionMultiplexer.Connect("localhost");
            Database = ConnectionMultiplexer.GetDatabase();
            Server = ConnectionMultiplexer.GetServer("localhost", 6379);
        }

        /// <summary>
        /// Set key to hold value.If key already holds value, it is overwritten, regardless ot its type.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis value</param>
        /// <param name="expireTime">redis expiry</param>
        /// <param name="when">when</param>
        /// <param name="commandFlags">command flags</param>
        /// <returns></returns>
        public async Task<bool> StringSetAsync(string key, object value, TimeSpan? expireTime = null,
            When when = When.Always, CommandFlags commandFlags = CommandFlags.None) =>
            await Database.StringSetAsync(key, JsonConvert.SerializeObject(value), expireTime, when, commandFlags);

        /// <summary>
        /// Set key to hold value.If key already holds value, it is overwritten, regardless ot its type.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis value</param>
        /// <returns></returns>
        public bool StringSet(string key, object value) => Database.StringSet(key, JsonConvert.SerializeObject(value));

        /// <summary>
        /// Get the value of key.If the key does not exist the special value nil is returned.
        /// An error is returned if the value stored at key is not string, because GET only handles string.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns></returns>
        public async Task<object> StringGetAsync(string key) =>
            JsonConvert.DeserializeObject(await Database.StringGetAsync(key));

        /// <summary>
        /// Get the value of key.If the key does not exist the special value nil is returned.
        /// An error is returned if the value stored at key is not string, because GET only handles string.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns></returns>
        public object StringGet(string key) => Database.StringGet(key);

        /// <summary>
        /// Return if key exists
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns>true if the key exist, false if key does not exist</returns>
        /// <returns></returns>
        public async Task<bool> KeyExistAsync(string key) => await Database.KeyExistsAsync(key);

        /// <summary>
        /// Return if key exists
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns>true if the key exist, false if key does not exist</returns>
        public bool KeyExist(string key) => Database.KeyExists(key);

        /// <summary>
        /// Removes the specified key.A key is ignored if it does not exist.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns></returns>
        public async Task<bool> KeyDeleteAsync(string key) => await Database.KeyDeleteAsync(key);

        /// <summary>
        /// Removes the specified key.A key is ignored if it does not exist.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns></returns>
        public bool KeyDelete(string key) => Database.KeyDelete(key);

        /// <summary>
        /// If key already exists and the key is string, this command appends the value at the end of the string.
        /// If key does not exist it is created and set as an empty value, so APPEND is similar to SET in some case.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis value</param>
        /// <returns></returns>
        public async Task<long> StringAppendAsync(string key, object value) =>
            await Database.StringAppendAsync(key, JsonConvert.SerializeObject(value));

        /// <summary>
        /// If key already exists and the key is string, this command appends the value at the end of the string.
        /// If key does not exist it is created and set as an empty value, so APPEND is similar to SET in some case.
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis value</param>
        /// <returns></returns>
        public long StringAppend(string key, object value) =>
            Database.StringAppend(key, JsonConvert.SerializeObject(value));

        /// <summary>
        /// Automatically sets key to value and returns the old value
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis new value</param>
        /// <returns>The old value stored at key, or nil when key did not exist</returns>
        public async Task<object> StringGetSetAsync(string key, object value) =>
            await Database.StringGetSetAsync(key, JsonConvert.SerializeObject(value));

        /// <summary>
        /// Automatically sets key to value and returns the old value
        /// </summary>
        /// <param name="key">redis key</param>
        /// <param name="value">redis new value</param>
        /// <returns>The old value stored at key, or nil when key did not exist</returns>
        public object StringGetSet(string key, object value) =>
            Database.StringGetSet(key, JsonConvert.SerializeObject(value));
    }
}
